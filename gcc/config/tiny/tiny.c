/*
   Copyright (C) 2012-2015 Free Software Foundation, Inc.
   Contributed by Red Hat.

   This file is part of GCC.

   GCC is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GCC is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GCC; see the file COPYING3.  If not see
   <http://www.gnu.org/licenses/>.  */

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "backend.h"
#include "target.h"
#include "rtl.h"
#include "tree.h"
#include "gimple-expr.h"
#include "df.h"
#include "tm_p.h"
#include "regs.h"
#include "emit-rtl.h"
#include "diagnostic-core.h"
#include "fold-const.h"
#include "stor-layout.h"
#include "calls.h"
#include "output.h"
#include "explow.h"
#include "expr.h"
#include "langhooks.h"
#include "builtins.h"

#include "target-def.h"

static void tiny_compute_frame_info (void);

/* Run-time Target Specification.  */
bool tinyx = true;

struct GTY(()) machine_function
{
  /* If set, the rest of the fields have been computed.  */
  int computed;
  /* Which registers need to be saved in the pro/epilogue.  */
  int need_to_save [FIRST_PSEUDO_REGISTER];

  /* These fields describe the frame layout...  */
  /* arg pointer */
  /* 2/4 bytes for saved PC */
  int framesize_regs;
  /* frame pointer */
  int framesize_locals;
  int framesize_outgoing;
  /* stack pointer */
  int framesize;
};

/* This is our init_machine_status, as set in
   msp_option_override.  */
static struct machine_function *
tiny_init_machine_status (void)
{
  struct machine_function *m;

  m = ggc_cleared_alloc<machine_function> ();

  return m;
}

#undef  TARGET_OPTION_OVERRIDE
#define TARGET_OPTION_OVERRIDE  tiny_option_override

static void
tiny_option_override (void)
{
  /* FIXME: do we need this here? */
  init_machine_status = tiny_init_machine_status;
  return;
}

/* Generate a C preprocessor symbol based upon the MCU selected by the user.
   If a specific MCU has not been selected then return a generic symbol instead.  */
const char *
tiny_mcu_name (void)
{
  if (target_mcu)
    {
      unsigned int i;
      static char mcu_name [64];

      snprintf (mcu_name, sizeof (mcu_name) - 1, "__%s__", target_mcu);
      for (i = strlen (mcu_name); i--;)
	mcu_name[i] = TOUPPER (mcu_name[i]);
      return mcu_name;
    }

  return "__TINY__";
}

#undef  TARGET_SCALAR_MODE_SUPPORTED_P
#define TARGET_SCALAR_MODE_SUPPORTED_P tiny_scalar_mode_supported_p

static bool
tiny_scalar_mode_supported_p (machine_mode m)
{
#if 0
  if (m == TImode)
    return true;
#endif
  return default_scalar_mode_supported_p (m);
}

/* Storage Layout */
#undef  TARGET_MS_BITFIELD_LAYOUT_P
#define TARGET_MS_BITFIELD_LAYOUT_P tiny_ms_bitfield_layout_p

bool
tiny_ms_bitfield_layout_p (const_tree record_type ATTRIBUTE_UNUSED)
{
  return false;
}

/* Register Usage */

/* Implements HARD_REGNO_NREGS.  TINYX registers can hold a single
   PSImode value, but not an SImode value.  */
int
tiny_hard_regno_nregs (int regno ATTRIBUTE_UNUSED,
			 machine_mode mode)
{
  return ((GET_MODE_SIZE (mode) + UNITS_PER_WORD - 1)
	  / UNITS_PER_WORD);
}

/* Implements HARD_REGNO_NREGS_HAS_PADDING.  */
int
tiny_hard_regno_nregs_has_padding (int regno ATTRIBUTE_UNUSED,
				     machine_mode mode)
{
  return ((GET_MODE_SIZE (mode) + UNITS_PER_WORD - 1)
	  / UNITS_PER_WORD);
}

/* Implements HARD_REGNO_NREGS_WITH_PADDING.  */
int
tiny_hard_regno_nregs_with_padding (int regno ATTRIBUTE_UNUSED,
				     machine_mode mode)
{
  return tiny_hard_regno_nregs (regno, mode);
}

/* Implements HARD_REGNO_MODE_OK.  */
int
tiny_hard_regno_mode_ok (int regno ATTRIBUTE_UNUSED,
			   machine_mode mode)
{
  return 1;

  return regno <= (ARG_POINTER_REGNUM - tiny_hard_regno_nregs (regno, mode));
}

/* Implements MODES_TIEABLE_P.  */
bool
tiny_modes_tieable_p (machine_mode mode1, machine_mode mode2)
{
  if ((mode1 == PSImode || mode2 == SImode)
      || (mode1 == SImode || mode2 == PSImode))
    return false;

  return ((GET_MODE_CLASS (mode1) == MODE_FLOAT
	   || GET_MODE_CLASS (mode1) == MODE_COMPLEX_FLOAT)
	  == (GET_MODE_CLASS (mode2) == MODE_FLOAT
	      || GET_MODE_CLASS (mode2) == MODE_COMPLEX_FLOAT));
}

#undef  TARGET_FRAME_POINTER_REQUIRED
#define TARGET_FRAME_POINTER_REQUIRED tiny_frame_pointer_required

static bool
tiny_frame_pointer_required (void)
{
  return false;
}

#undef  TARGET_CAN_ELIMINATE
#define TARGET_CAN_ELIMINATE		tiny_can_eliminate

static bool
tiny_can_eliminate (const int from_reg ATTRIBUTE_UNUSED,
		      const int to_reg ATTRIBUTE_UNUSED)
{
    if ((from_reg == FRAME_POINTER_REGNUM && to_reg == HARD_FRAME_POINTER_REGNUM) ||
        (from_reg == FRAME_POINTER_REGNUM && to_reg == STACK_POINTER_REGNUM) ||
        (from_reg == ARG_POINTER_REGNUM && to_reg == HARD_FRAME_POINTER_REGNUM) ||
        (from_reg == ARG_POINTER_REGNUM && to_reg == STACK_POINTER_REGNUM))
        return true;
    else
        return false;
}

int
tiny_starting_frame_offset() {
    return 0;
    if (!cfun->machine->computed)
        tiny_compute_frame_info ();

    printf("%d\n", get_frame_size());
    gcc_assert(0);

    return cfun->machine->framesize_regs;
}

/* Implements INITIAL_ELIMINATION_OFFSET.  */
int
tiny_initial_elimination_offset (int from, int to)
{
  int rv = 0; /* As if arg to arg.  */

  tiny_compute_frame_info ();

  if (from == to)
    return 0;

  DEBUG_GCC(fprintf(stderr, "FRAME: from %d to %d locals %d regs %d\n", from, to,
    cfun->machine->framesize_locals, cfun->machine->framesize_regs););

  if (from == FRAME_POINTER_REGNUM && to == STACK_POINTER_REGNUM) {
    return cfun->machine->framesize;
  } else if (from == FRAME_POINTER_REGNUM && to == HARD_FRAME_POINTER_REGNUM) {
    return -(cfun->machine->framesize_regs);
  } else if (from == ARG_POINTER_REGNUM && to == FRAME_POINTER_REGNUM) {
    return 4;
  } else if (from == ARG_POINTER_REGNUM && to == HARD_FRAME_POINTER_REGNUM) {
    return 4;
  } else if (from == ARG_POINTER_REGNUM && to == STACK_POINTER_REGNUM) {
    return cfun->machine->framesize_locals + 4;
  } else {
    fprintf(stderr, "%d %d\n", from, to);
    gcc_assert(0 && "tiny_initial_elimination_offset invalid combination");
  }
#if 0
  switch (to)
    {
    case STACK_POINTER_REGNUM:
      rv += cfun->machine->framesize_outgoing;
      rv += cfun->machine->framesize_locals;
      /* fall through.  */
    case FRAME_POINTER_REGNUM:
      rv += cfun->machine->framesize_regs;
      /* Allow for the saved return address.  */
      rv += 4;
      /* NB/ No need to allow for crtl->args.pretend_args_size.
         GCC does that for us.  */
      break;
    default:
      gcc_unreachable ();
    }

  switch (from)
    {
    case FRAME_POINTER_REGNUM:
      /* Allow for the fall through above.  */
      rv -= 4;
      rv -= cfun->machine->framesize_regs;
      break;
    //case ARG_POINTER_REGNUM:
    //  break;
    default:
      gcc_unreachable ();
    }

  return rv;
#endif
}

/* Named Address Space support */

/* Return the appropriate mode for a named address pointer.  */
#undef  TARGET_ADDR_SPACE_POINTER_MODE
#define TARGET_ADDR_SPACE_POINTER_MODE tiny_addr_space_pointer_mode
#undef  TARGET_ADDR_SPACE_ADDRESS_MODE
#define TARGET_ADDR_SPACE_ADDRESS_MODE tiny_addr_space_pointer_mode

static machine_mode
tiny_addr_space_pointer_mode (addr_space_t addrspace)
{
  switch (addrspace)
    {
    default:
    case ADDR_SPACE_GENERIC:
    case ADDR_SPACE_NEAR:
    case ADDR_SPACE_FAR:
      return SImode;
    }
}

/* Function pointers are stored in unwind_word sized
   variables, so make sure that unwind_word is big enough.  */
#undef  TARGET_UNWIND_WORD_MODE
#define TARGET_UNWIND_WORD_MODE tiny_unwind_word_mode

static machine_mode
tiny_unwind_word_mode (void)
{
  return SImode;
  /* return TARGET_LARGE ? PSImode : HImode; */
}

/* Determine if one named address space is a subset of another.  */
#undef  TARGET_ADDR_SPACE_SUBSET_P
#define TARGET_ADDR_SPACE_SUBSET_P tiny_addr_space_subset_p
static bool
tiny_addr_space_subset_p (addr_space_t subset, addr_space_t superset)
{
  if (subset == superset)
    return true;
  else
    return (subset != ADDR_SPACE_FAR && superset == ADDR_SPACE_FAR);
}

#undef  TARGET_ADDR_SPACE_CONVERT
#define TARGET_ADDR_SPACE_CONVERT tiny_addr_space_convert
/* Convert from one address space to another.  */
static rtx
tiny_addr_space_convert (rtx op, tree from_type, tree to_type)
{
  addr_space_t from_as = TYPE_ADDR_SPACE (TREE_TYPE (from_type));
  addr_space_t to_as = TYPE_ADDR_SPACE (TREE_TYPE (to_type));
  rtx result;

  /* FIXME: do we need this?
   */
  gcc_unreachable ();

  if (to_as != ADDR_SPACE_FAR && from_as == ADDR_SPACE_FAR)
    {
      /* This is unpredictable, as we're truncating off usable address
	 bits.  */

      if (CONSTANT_P (op))
	return gen_rtx_CONST (HImode, op);

      result = gen_reg_rtx (HImode);
      emit_insn (gen_truncsihi2 (result, op));
      return result;
    }
  else if (to_as == ADDR_SPACE_FAR && from_as != ADDR_SPACE_FAR)
    {
      /* This always works.  */

      if (CONSTANT_P (op))
	return gen_rtx_CONST (PSImode, op);

      result = gen_reg_rtx (PSImode);
      emit_insn (gen_zero_extendhisi2 (result, op));
      return result;
    }
  else
    gcc_unreachable ();
}

/* Stack Layout and Calling Conventions.  */

/* For each function, we list the gcc version and the TI version on
   each line, where we're converting the function names.  */
static char const * const special_convention_function_names [] =
{
  "__muldi3", "__mspabi_mpyll",
  "__udivdi3", "__mspabi_divull",
  "__umoddi3", "__mspabi_remull",
  "__divdi3", "__mspabi_divlli",
  "__moddi3", "__mspabi_remlli",
  "__mspabi_srall",
  "__mspabi_srlll",
  "__mspabi_sllll",
  "__adddf3", "__mspabi_addd",
  "__subdf3", "__mspabi_subd",
  "__muldf3", "__mspabi_mpyd",
  "__divdf3", "__mspabi_divd",
  "__mspabi_cmpd",
  NULL
};

/* TRUE if the function passed is a "speical" function.  Special
   functions pass two DImode parameters in registers.  */
static bool
tiny_special_register_convention_p (const char *name)
{
  int i;

  for (i = 0; special_convention_function_names [i]; i++)
    if (! strcmp (name, special_convention_function_names [i]))
      return true;

  return false;
}

#undef  TARGET_FUNCTION_VALUE_REGNO_P
#define TARGET_FUNCTION_VALUE_REGNO_P tiny_function_value_regno_p

bool
tiny_function_value_regno_p (unsigned int regno)
{
  return regno == 0;
}


#undef  TARGET_FUNCTION_VALUE
#define TARGET_FUNCTION_VALUE tiny_function_value

rtx
tiny_function_value (const_tree ret_type,
		       const_tree fn_decl_or_type ATTRIBUTE_UNUSED,
		       bool outgoing ATTRIBUTE_UNUSED)
{
  return gen_rtx_REG (TYPE_MODE (ret_type), 0);
}

#undef  TARGET_LIBCALL_VALUE
#define TARGET_LIBCALL_VALUE tiny_libcall_value

rtx
tiny_libcall_value (machine_mode mode, const_rtx fun ATTRIBUTE_UNUSED)
{
  DEBUG_GCC(debug_rtx(fun));
  gcc_assert(0);
  return gen_rtx_REG (mode, 1);
}

/* implements INIT_CUMULATIVE_ARGS */
void
tiny_init_cumulative_args (CUMULATIVE_ARGS *ca,
			     tree fntype ATTRIBUTE_UNUSED,
			     rtx libname ATTRIBUTE_UNUSED,
			     tree fndecl ATTRIBUTE_UNUSED,
			     int n_named_args ATTRIBUTE_UNUSED)
{
  /* this hook should initialize CUMULATIVE_ARGS struct
   * before it can be passed to other macros/hooks */
}

/* Helper function for argument passing; this function is the common
   code that determines where an argument will be passed.  */
static void
tiny_evaluate_arg (cumulative_args_t cap,
		     machine_mode mode,
		     const_tree type ATTRIBUTE_UNUSED,
		     bool named)
{
  fprintf(stderr, "tiny_evaluate_arg called\n");
  return;
}

#undef  TARGET_PROMOTE_PROTOTYPES
#define TARGET_PROMOTE_PROTOTYPES tiny_promote_prototypes

bool
tiny_promote_prototypes (const_tree fntype ATTRIBUTE_UNUSED)
{
  return true;
}

#undef  TARGET_FUNCTION_ARG
#define TARGET_FUNCTION_ARG tiny_function_arg

rtx
tiny_function_arg (cumulative_args_t cap,
		     machine_mode mode,
		     const_tree type,
		     bool named)
{
  /* this is tha main ARG* hook which decides if argument
   * is passed in register or in memory
   * we pass everything on stack */
  return NULL_RTX;
}

#undef  TARGET_ARG_PARTIAL_BYTES
#define TARGET_ARG_PARTIAL_BYTES tiny_arg_partial_bytes

int
tiny_arg_partial_bytes (cumulative_args_t cap,
			  machine_mode mode,
			  tree type,
			  bool named)
{
  /* return number of bytes of argument that is passed in
   * register the rest is passed on stack */
  return 0;
}

#undef  TARGET_PASS_BY_REFERENCE
#define TARGET_PASS_BY_REFERENCE tiny_pass_by_reference

static bool
tiny_pass_by_reference (cumulative_args_t cap ATTRIBUTE_UNUSED,
			  machine_mode mode,
			  const_tree type,
			  bool named ATTRIBUTE_UNUSED)
{
  return (mode == BLKmode
	  || (type && TREE_CODE (type) == RECORD_TYPE)
	  || (type && TREE_CODE (type) == UNION_TYPE));
}

#undef  TARGET_CALLEE_COPIES
#define TARGET_CALLEE_COPIES tiny_callee_copies

static bool
tiny_callee_copies (cumulative_args_t cap ATTRIBUTE_UNUSED,
                     machine_mode mode ATTRIBUTE_UNUSED,
                     const_tree type ATTRIBUTE_UNUSED,
                     bool named ATTRIBUTE_UNUSED)
{
  return false;
}

#undef  TARGET_FUNCTION_ARG_ADVANCE
#define TARGET_FUNCTION_ARG_ADVANCE tiny_function_arg_advance

void
tiny_function_arg_advance (cumulative_args_t cap,
			     machine_mode mode,
			     const_tree type,
			     bool named)
{
  /* updates the information stored in our cap struct so
   * it's ready to be used in next TARGET_FUNCTION_ARG */
  return;
}

#undef  TARGET_FUNCTION_ARG_BOUNDARY
#define TARGET_FUNCTION_ARG_BOUNDARY tiny_function_arg_boundary

static unsigned int
tiny_function_arg_boundary (machine_mode mode, const_tree type)
{
  if (mode == BLKmode
      && int_size_in_bytes (type) > 1)
    return 16;
  if (GET_MODE_BITSIZE (mode) > 8)
    return 16;
  return 8;
}

#undef  TARGET_RETURN_IN_MEMORY
#define TARGET_RETURN_IN_MEMORY tiny_return_in_memory

static bool
tiny_return_in_memory (const_tree ret_type, const_tree fntype ATTRIBUTE_UNUSED)
{
  machine_mode mode = TYPE_MODE (ret_type);

  if (mode == BLKmode
      || (fntype && TREE_CODE (TREE_TYPE (fntype)) == RECORD_TYPE)
      || (fntype && TREE_CODE (TREE_TYPE (fntype)) == UNION_TYPE))
    return true;

  if (GET_MODE_SIZE (mode) > 8)
    return true;

  return false;
}

#undef  TARGET_GET_RAW_ARG_MODE
#define TARGET_GET_RAW_ARG_MODE tiny_get_raw_arg_mode

static machine_mode
tiny_get_raw_arg_mode (int regno)
{
  return (regno == ARG_POINTER_REGNUM) ? VOIDmode : Pmode;
}

#undef  TARGET_GET_RAW_RESULT_MODE
#define TARGET_GET_RAW_RESULT_MODE tiny_get_raw_result_mode

static machine_mode
tiny_get_raw_result_mode (int regno ATTRIBUTE_UNUSED)
{
  return Pmode;
}

#undef  TARGET_GIMPLIFY_VA_ARG_EXPR
#define TARGET_GIMPLIFY_VA_ARG_EXPR tiny_gimplify_va_arg_expr

#include "gimplify.h"
#include "gimple-expr.h"

static tree
tiny_gimplify_va_arg_expr (tree valist, tree type, gimple_seq *pre_p,
			  gimple_seq *post_p)
{
  gcc_assert(0 && "we don't support varargs yet");
}
/* Addressing Modes */

#undef  TARGET_LEGITIMATE_ADDRESS_P
#define TARGET_LEGITIMATE_ADDRESS_P tiny_legitimate_address_p

static bool
reg_ok_for_addr (rtx r, bool strict)
{
  int rn = REGNO (r);

  if (strict && rn >= FIRST_PSEUDO_REGISTER)
    rn = reg_renumber [rn];
  if (strict && 0 <= rn && rn < FIRST_PSEUDO_REGISTER)
    return true;
  if (!strict)
    return true;
  return false;
}

bool
tiny_legitimate_address_p (machine_mode mode ATTRIBUTE_UNUSED,
			     rtx x ATTRIBUTE_UNUSED,
			     bool strict ATTRIBUTE_UNUSED)
{
  DEBUG_GCC(
    fprintf(stderr, "tiny_legitimate_address_p: \n");
    debug_rtx (x);
    fprintf(stderr, "mode %s\n", GET_MODE_NAME(mode));
  );
  switch (GET_CODE (x))
    {
    case MEM:
      goto fail;

    case PLUS:
      DEBUG_GCC(fprintf (stderr, "plus\n"));
      if (REG_P (XEXP (x, 0)))
	{
      DEBUG_GCC(fprintf (stderr, "REG_P\n"));
	  if (GET_MODE (x) != GET_MODE (XEXP (x, 0)))
        goto fail;
	  if (!reg_ok_for_addr (XEXP (x, 0), strict))
        goto fail;
	  switch (GET_CODE (XEXP (x, 1)))
	    {
	    case CONST:
	    case CONST_INT:
	      goto ok;
	    default:
          goto fail;
	    }
	}
      goto fail;

    case REG:
      if (!reg_ok_for_addr (x, strict))
        goto fail;
      else
        goto ok;;

    case CONST_INT:
    default:
      goto fail;
    }

fail:
    DEBUG_GCC(fprintf(stderr, "failed\n\n"));
    return false;
ok:
    DEBUG_GCC(fprintf(stderr, "ok\n\n"));
    return true;
}

#undef  TARGET_ADDR_SPACE_LEGITIMATE_ADDRESS_P
#define TARGET_ADDR_SPACE_LEGITIMATE_ADDRESS_P tiny_addr_space_legitimate_address_p

bool
tiny_addr_space_legitimate_address_p (machine_mode mode,
					rtx x,
					bool strict,
					addr_space_t as ATTRIBUTE_UNUSED)
{
  return tiny_legitimate_address_p (mode, x, strict);
}

#undef  TARGET_ASM_INTEGER
#define TARGET_ASM_INTEGER tiny_asm_integer
static bool
tiny_asm_integer (rtx x, unsigned int size, int aligned_p)
{
  int c = GET_CODE (x);

  if (size == 3 && GET_MODE (x) == PSImode)
    size = 4;

  switch (size)
    {
    case 4:
      if (c == SYMBOL_REF || c == CONST || c == LABEL_REF || c == CONST_INT)
	{
	  fprintf (asm_out_file, "\t.long\t");
	  output_addr_const (asm_out_file, x);
	  fputc ('\n', asm_out_file);
	  return true;
	}
      break;
    }
  return default_assemble_integer (x, size, aligned_p);
}

#undef  TARGET_ASM_OUTPUT_ADDR_CONST_EXTRA
#define TARGET_ASM_OUTPUT_ADDR_CONST_EXTRA tiny_asm_output_addr_const_extra
static bool
tiny_asm_output_addr_const_extra (FILE *file ATTRIBUTE_UNUSED, rtx x)
{
  DEBUG_GCC(debug_rtx(x));
  return false;
}

#undef  TARGET_LEGITIMATE_CONSTANT_P
#define TARGET_LEGITIMATE_CONSTANT_P tiny_legitimate_constant

static bool
tiny_legitimate_constant (machine_mode mode, rtx x)
{
  /* we have to allow constants and labels (sybolic constants) */
  if (CONST_INT_P (x) || SYMBOL_REF == GET_CODE (x))
    return true;

  return false;
}

#undef  TARGET_RTX_COSTS
#define TARGET_RTX_COSTS tiny_rtx_costs

static bool tiny_rtx_costs (rtx   x ATTRIBUTE_UNUSED,
			      machine_mode mode,
			      int   outer_code ATTRIBUTE_UNUSED,
			      int   opno ATTRIBUTE_UNUSED,
			      int * total,
			      bool  speed ATTRIBUTE_UNUSED)
{
  int code = GET_CODE(x);
  switch (code)
    {
/*
    case SIGN_EXTEND:
      if (GET_MODE (x) == SImode && outer_code == SET)
	{
	  *total = COSTS_N_INSNS (4);
	  return true;
	}
      break;
*/
    case PLUS:
    case MINUS:
        *total = COSTS_N_INSNS (6);
      break;
/* wo don't have problem with shifts
    case ASHIFT:
    case ASHIFTRT:
    case LSHIFTRT:
      if (!tinyx)
	{
	  *total = COSTS_N_INSNS (100);
	  return true;
	}
      break;
*/
    }
  return false;
}

/* Function Entry and Exit */

/* The TINY call frame looks like this:

   <higher addresses>
   +--------------------+
   |                    |
   | Stack Arguments    |
   |                    |
   +--------------------+ <-- "arg pointer"
   |                    |
   | PC from call       |  (2 bytes for 430, 4 for TARGET_LARGE)
   |                    |
   +--------------------+
   | SR if this func has|
   | been called via an |
   | interrupt.         |
   +--------------------+  <-- SP before prologue, also AP
   |                    |
   | Saved Regs         |  (2 bytes per reg for 430, 4 per for TARGET_LARGE)
   |                    |
   +--------------------+  <-- "frame pointer"
   |                    |
   | Locals             |
   |                    |
   +--------------------+
   |                    |
   | Outgoing Args      |
   |                    |
   +--------------------+  <-- SP during function
   <lower addresses>

*/

/* We use this to wrap all emitted insns in the prologue, so they get
   the "frame-related" (/f) flag set.  */
static rtx
F (rtx x)
{
  RTX_FRAME_RELATED_P (x) = 1;
  return x;
}

/* This is the one spot that decides if a register is to be saved and
   restored in the prologue/epilogue.  */
static bool
tiny_preserve_reg_p (int regno)
{
  /* if REGNO is not caller-saved and is live
     in current function we have to save if */
  if (!call_used_regs [regno]
      && df_regs_ever_live_p (regno)) {
    DEBUG_GCC(fprintf(stderr, "FRAME: preserve %d\n", regno));
    return true;
  }

  DEBUG_GCC(fprintf(stderr, "FRAME: not preserve %d callee %d used %d\n", regno,
    call_used_regs[regno], df_regs_ever_live_p(regno)));
  return false;
}

/* Compute all the frame-related fields in our machine_function
   structure.  */
static void
tiny_compute_frame_info (void)
{
  int i;

  cfun->machine->computed = 1;
  cfun->machine->framesize_regs = 0;
  cfun->machine->framesize_locals = get_frame_size ();
  cfun->machine->framesize_outgoing = crtl->outgoing_args_size;

  /* count area for callee saved regs */
  for (i = 0; i < FIRST_PSEUDO_REGISTER; i ++) {
    if (tiny_preserve_reg_p (i)) {
        DEBUG_GCC(fprintf(stderr, "FRAME: needs save %d\n" , i););
	  cfun->machine->need_to_save [i] = 1;
	  cfun->machine->framesize_regs += 4;
    } else {
      cfun->machine->need_to_save [i] = 0;
    }
  }

  if ((cfun->machine->framesize_locals + cfun->machine->framesize_outgoing) & 1)
    cfun->machine->framesize_locals ++;

  cfun->machine->framesize = (cfun->machine->framesize_regs
			      + cfun->machine->framesize_locals
			      + cfun->machine->framesize_outgoing);
}

static inline bool
is_attr_func (const char * attr)
{
  return lookup_attribute (attr, DECL_ATTRIBUTES (current_function_decl)) != NULL_TREE;
}

/* Returns true if the current function has the "interrupt" attribute.  */

bool
tiny_is_interrupt_func (void)
{
  if (current_function_decl == NULL)
    return false;
  return is_attr_func ("interrupt");
}

static bool
is_wakeup_func (void)
{
  return tiny_is_interrupt_func () && is_attr_func ("wakeup");
}

static inline bool
is_naked_func (void)
{
  return is_attr_func ("naked");
}

static inline bool
is_reentrant_func (void)
{
  return is_attr_func ("reentrant");
}

static inline bool
is_critical_func (void)
{
  return is_attr_func ("critical");
}

#undef  TARGET_ASM_FUNCTION_PROLOGUE
#define TARGET_ASM_FUNCTION_PROLOGUE	tiny_start_function

static void
tiny_start_function (FILE *outfile, HOST_WIDE_INT hwi_local ATTRIBUTE_UNUSED)
{
  int r, n;

  fprintf (outfile, "; start of function\n");

  if (DECL_ATTRIBUTES (current_function_decl) != NULL_TREE)
    {
      fprintf (outfile, "; attributes: ");
      if (is_naked_func ())
	fprintf (outfile, "naked ");
      if (tiny_is_interrupt_func ())
	fprintf (outfile, "interrupt ");
      if (is_reentrant_func ())
	fprintf (outfile, "reentrant ");
      if (is_critical_func ())
	fprintf (outfile, "critical ");
      if (is_wakeup_func ())
	fprintf (outfile, "wakeup ");
      fprintf (outfile, "\n");
    }

  fprintf (outfile, "; framesize_regs:     %d\n", cfun->machine->framesize_regs);
  fprintf (outfile, "; framesize_locals:   %d\n", cfun->machine->framesize_locals);
  fprintf (outfile, "; framesize_outgoing: %d\n", cfun->machine->framesize_outgoing);
  fprintf (outfile, "; framesize:          %d\n", cfun->machine->framesize);
  //fprintf (outfile, "; elim ap -> fp       %d\n", tiny_initial_elimination_offset (ARG_POINTER_REGNUM, FRAME_POINTER_REGNUM));
  //fprintf (outfile, "; elim fp -> sp       %d\n", tiny_initial_elimination_offset (FRAME_POINTER_REGNUM, STACK_POINTER_REGNUM));

  n = 0;
  fprintf (outfile, "; saved regs:");
  for (r = 0; r <= FRAME_POINTER_REGNUM; r++)
    if (cfun->machine->need_to_save [r])
      {
	fprintf (outfile, " %s", reg_names [r]);
	n = 1;
      }
  if (n == 0)
    fprintf (outfile, "(none)");
  fprintf (outfile, "\n");
}

/* Common code to change the stack pointer.  */
static void
increment_stack (HOST_WIDE_INT amount)
{
  rtx inc;
  rtx sp =  stack_pointer_rtx;

  if (amount == 0)
    return;

  rtx tmp_reg = gen_rtx_REG (SImode, 1);
  if (amount < 0) {
      inc = GEN_INT (- amount);
      emit_insn (gen_movsi (tmp_reg, inc));
	  F (emit_insn (gen_subsi3 (sp, sp, tmp_reg)));
  } else {
      inc = GEN_INT (amount);
      emit_insn (gen_movsi (tmp_reg, inc));
	  F (emit_insn (gen_addsi3 (sp, sp, tmp_reg)));
  }
}

/* Verify TINY specific attributes.  */

static tree
tiny_attr (tree * node,
	     tree   name,
	     tree   args,
	     int    flags ATTRIBUTE_UNUSED,
	     bool * no_add_attrs)
{
  gcc_assert (DECL_P (* node));

  if (args != NULL)
    {
      tree value = TREE_VALUE (args);

      switch (TREE_CODE (value))
	{
	case STRING_CST:
	  if (   strcmp (TREE_STRING_POINTER (value), "reset")
	      && strcmp (TREE_STRING_POINTER (value), "nmi")
	      && strcmp (TREE_STRING_POINTER (value), "watchdog"))
	    /* Allow the attribute to be added - the linker script
	       being used may still recognise this name.  */
	    warning (OPT_Wattributes,
		     "unrecognised interrupt vector argument of %qE attribute",
		     name);
	  break;

	case INTEGER_CST:
	  if (wi::gtu_p (value, 63))
	    /* Allow the attribute to be added - the linker script
	       being used may still recognise this value.  */
	    warning (OPT_Wattributes,
		     "numeric argument of %qE attribute must be in range 0..63",
		     name);
	  break;

	default:
	  warning (OPT_Wattributes,
		   "argument of %qE attribute is not a string constant or number",
		   name);
	  *no_add_attrs = true;
	  break;
	}
    }

  if (TREE_CODE (* node) != FUNCTION_DECL)
    {
      warning (OPT_Wattributes,
	       "%qE attribute only applies to functions",
	       name);
      * no_add_attrs = true;
    }

  /* FIXME: We ought to check that the interrupt handler
     attribute has been applied to a void function.  */
  /* FIXME: We should check that reentrant and critical
     functions are not naked and that critical functions
     are not reentrant.  */

  return NULL_TREE;
}

#undef  TARGET_ATTRIBUTE_TABLE
#define TARGET_ATTRIBUTE_TABLE		tiny_attribute_table

/* Table of TINY-specific attributes.  */
const struct attribute_spec tiny_attribute_table[] =
{
  /* Name          min_len  decl_req,    fn_type_req,    affects_type_identity
                       max_len,  type_req,        handler.  */
  { "interrupt",      0, 1, true,  false, false, tiny_attr, false },
  { "naked",          0, 0, true,  false, false, tiny_attr, false },
  { "reentrant",      0, 0, true,  false, false, tiny_attr, false },
  { "critical",       0, 0, true,  false, false, tiny_attr, false },
  { "wakeup",         0, 0, true,  false, false, tiny_attr, false },
  { NULL,             0, 0, false, false, false, NULL,        false }
};

void
tiny_start_function (FILE *file, const char *name, tree decl)
{
  tree int_attr;

  int_attr = lookup_attribute ("interrupt", DECL_ATTRIBUTES (decl));
  if (int_attr != NULL_TREE)
    {
      tree intr_vector = TREE_VALUE (int_attr);

      if (intr_vector != NULL_TREE)
	{
	  char buf[101];

	  intr_vector = TREE_VALUE (intr_vector);

	  /* The interrupt attribute has a vector value.  Turn this into a
	     section name, switch to that section and put the address of
	     the current function into that vector slot.  Note tiny_attr()
	     has already verified the vector name for us.  */
	  if (TREE_CODE (intr_vector) == STRING_CST)
	    sprintf (buf, "__interrupt_vector_%.80s",
		     TREE_STRING_POINTER (intr_vector));
	  else /* TREE_CODE (intr_vector) == INTEGER_CST */
	    sprintf (buf, "__interrupt_vector_%u",
		     (unsigned int) TREE_INT_CST_LOW (intr_vector));

	  switch_to_section (get_section (buf, SECTION_CODE, decl));
	  fputs ("\t.word\t", file);
	  assemble_name (file, name);
	  fputc ('\n', file);
	  fputc ('\t', file);
	}
    }

  switch_to_section (function_section (decl));
  ASM_OUTPUT_FUNCTION_LABEL (file, name, decl);
}

static section *
tiny_function_section (tree decl, enum node_frequency freq, bool startup, bool exit)
{
  /* In large mode we must make sure that interrupt handlers are put into
     low memory as the vector table only accepts 16-bit addresses.  */
  if (TARGET_LARGE
      && lookup_attribute ("interrupt", DECL_ATTRIBUTES (decl)))
    return get_section (".lowtext", SECTION_CODE | SECTION_WRITE , decl);

  /* Otherwise, use the default function section.  */
  return default_function_section (decl, freq, startup, exit);
}

#undef  TARGET_ASM_FUNCTION_SECTION
#define TARGET_ASM_FUNCTION_SECTION tiny_function_section

enum tiny_builtin
{
  TINY_BUILTIN_IN,
  TINY_BUILTIN_OUT,
  TINY_BUILTIN_max
};

static GTY(()) tree tiny_builtins [(int) TINY_BUILTIN_max];

static void
tiny_init_builtins (void)
{
  tree void_ftype_int = build_function_type_list (void_type_node, integer_type_node, NULL);
  tree int_ftype_void = build_function_type_list (integer_type_node, void_type_node, NULL);

  tiny_builtins[TINY_BUILTIN_OUT] =
    add_builtin_function ( "__out", void_ftype_int,
			   TINY_BUILTIN_OUT, BUILT_IN_MD, NULL, NULL_TREE);

  tiny_builtins[TINY_BUILTIN_IN] =
    add_builtin_function ( "__in", int_ftype_void,
               TINY_BUILTIN_IN, BUILT_IN_MD, NULL, NULL_TREE);

}

static tree
tiny_builtin_decl (unsigned code, bool initialize ATTRIBUTE_UNUSED)
{
  switch (code)
    {
    case TINY_BUILTIN_OUT:
    case TINY_BUILTIN_IN:
      return tiny_builtins[code];
    default:
      return error_mark_node;
    }
}

static rtx
tiny_expand_builtin (tree exp,
		       rtx target ATTRIBUTE_UNUSED,
		       rtx subtarget ATTRIBUTE_UNUSED,
		       machine_mode mode ATTRIBUTE_UNUSED,
		       int ignore ATTRIBUTE_UNUSED)
{
  tree fndecl = TREE_OPERAND (CALL_EXPR_FN (exp), 0);
  unsigned int fcode = DECL_FUNCTION_CODE (fndecl);
/*
  if (! REG_P (arg1) && ! CONSTANT_P (arg1))
  printf("expand_builtin: GET_MODE(mode): %s\n", GET_MODE_NAME (mode));
  printf("expand_builtin: GET_MODE(mode): %s\n", GET_MODE_NAME ( GET_MODE (arg1)));
 */
  switch (fcode)
    {
    case TINY_BUILTIN_OUT:
        {
        fprintf(stderr, "TINY_BUILTIN_OUT\n");
        rtx arg1 = expand_normal (CALL_EXPR_ARG (exp, 0));
        debug_rtx(arg1);
        //arg1 = force_reg (GET_MODE (arg1), arg1);
        arg1 = force_reg (SImode, arg1);
        debug_rtx(arg1);
        emit_insn (gen_out (arg1));
        }
      break;
    case TINY_BUILTIN_IN:
        {
        rtx ret = force_reg (GET_MODE (target), target);
        emit_insn (gen_in (ret));
        return ret;
        }
      break;
    default:
      internal_error ("bad builtin code");
      break;
    }
  return NULL_RTX;
}

#undef  TARGET_INIT_BUILTINS
#define TARGET_INIT_BUILTINS  tiny_init_builtins

#undef  TARGET_EXPAND_BUILTIN
#define TARGET_EXPAND_BUILTIN tiny_expand_builtin

#undef  TARGET_BUILTIN_DECL
#define TARGET_BUILTIN_DECL   tiny_builtin_decl

void tiny_expand_prologue (void) {

  int i, j;
  int fs;
  /* Always use stack_pointer_rtx instead of calling
     rtx_gen_REG ourselves.  Code elsewhere in GCC assumes
     that there is a single rtx representing the stack pointer,
     namely stack_pointer_rtx, and uses == to recognize it.  */
  rtx sp = stack_pointer_rtx;
  rtx p;

  if (is_naked_func ()) {
      fprintf (stderr, "UNSUPPORTED: we don't support naked functions");
      gcc_unreachable ();
  }

  if (is_critical_func ()) {
      fprintf (stderr, "UNSUPPORTED: we don't support critical functions");
      gcc_unreachable ();
  }

  if (is_reentrant_func ()) {
      fprintf (stderr, "UNSUPPORTED: we don't support reentrant functions");
      gcc_unreachable ();
  }

  emit_insn (gen_prologue_start_marker ());

  /* compute frame information */
  if (!cfun->machine->computed)
    tiny_compute_frame_info ();

  /* for debugging see -fstack-isage */
  if (flag_stack_usage_info)
    current_function_static_stack_size = cfun->machine->framesize;

  if (crtl->args.pretend_args_size) {

      /* what is this? */
      gcc_unreachable();
      rtx note;

      gcc_assert (crtl->args.pretend_args_size == 2);

      p = NULL;
      /* emit_insn (gen_grow_and_swap ()); */

      /* Document the stack decrement...  */
      note = F (gen_rtx_SET (stack_pointer_rtx,
			     gen_rtx_MINUS (Pmode, stack_pointer_rtx, GEN_INT (2))));
      add_reg_note (p, REG_FRAME_RELATED_EXPR, note);

      /* ...and the establishment of a new location for the return address.  */
      note = F (gen_rtx_SET (gen_rtx_MEM (Pmode,
						 gen_rtx_PLUS (Pmode, stack_pointer_rtx, GEN_INT (-2))),
			     pc_rtx));
      add_reg_note (p, REG_CFA_OFFSET, note);
      F (p);
  }

  /* total size of stack frame */
  fs = cfun->machine->framesize_locals + cfun->machine->framesize_outgoing
        + cfun->machine->framesize_regs;

  /* if frame pointer is used we have to save its content */
  if (frame_pointer_needed) {
    rtx addr = gen_rtx_PLUS (SImode, sp, GEN_INT (-4));
    F (emit_insn (gen_rtx_SET (gen_rtx_MEM (SImode, addr),
                                gen_rtx_REG (SImode, HARD_FRAME_POINTER_REGNUM))));

    DEBUG_GCC(fprintf (stderr, "FRAME: frame_pointer_needed\n"));
    F (emit_move_insn (gen_rtx_REG (Pmode, HARD_FRAME_POINTER_REGNUM), sp));
  }

  /* emit stack allocation code */
  increment_stack (- fs);

  int offset = 8;
  for (i = 0; i < FRAME_POINTER_REGNUM; i++) {
    if (cfun->machine->need_to_save [i]) {
        if (i == HARD_FRAME_POINTER_REGNUM)
            continue;
 #if 0
	if (tinyx) {
	    /* Note: with TARGET_LARGE we still use PUSHM as PUSHX.A is two bytes bigger.  */
	    p = F (emit_insn (gen_pushm (gen_rtx_REG (Pmode, i),
					 GEN_INT (count))));
	    note = gen_rtx_SEQUENCE (VOIDmode, rtvec_alloc (count + 1));
	    XVECEXP (note, 0, 0)
	      = F (gen_rtx_SET (VOIDmode,
			     stack_pointer_rtx,
			     gen_rtx_PLUS (Pmode,
					   stack_pointer_rtx,
					   GEN_INT (count * (TARGET_LARGE ? -4 : -2)))));
	    /* *sp-- = R[i-j] */
	    /* sp+N	R10
	       ...
	       sp	R4  */
	    for (j = 0; j < count; j ++) {
		    rtx addr;
		    int ofs = (count - j - 1) * (TARGET_LARGE ? 4 : 2);
		    if (ofs)
		      addr = gen_rtx_PLUS (Pmode, sp, GEN_INT (ofs));
		    else
		      addr = stack_pointer_rtx;
		    XVECEXP (note, 0, j + 1) =
		      F (gen_rtx_SET (VOIDmode,
		    		  gen_rtx_MEM (Pmode, addr),
		    		  gen_rtx_REG (Pmode, i - j)) );
	    }
	    add_reg_note (p, REG_FRAME_RELATED_EXPR, note);
	    i -= count - 1;
	  }
	else
	  F (emit_insn (gen_push (gen_rtx_REG (Pmode, i))));
#endif
    rtx addr = gen_rtx_PLUS (SImode, sp, GEN_INT (cfun->machine->framesize - offset));
    //rtx addr = gen_rtx_PLUS (SImode, sp, GEN_INT (offset));
    F (emit_insn (gen_rtx_SET (gen_rtx_MEM (SImode, addr),
                                gen_rtx_REG (SImode, i))));
    offset += 4;
    }
  }

  emit_insn (gen_prologue_end_marker ());
}

void
tiny_expand_epilogue (int is_eh) {

  int i;
  int fs;
  int helper_n = 0;

  if (is_naked_func ()) {
      fprintf (stderr, "UNSUPPORTED: we don't support naked functions");
      gcc_unreachable ();
  }

  /*
  if (cfun->machine->need_to_save [10]) {
      Check for a helper function. 
      helper_n = 7; For when the loop below never sees a match. 
      for (i = 9; i >= 4; i--)
	if (!cfun->machine->need_to_save [i])
	  {
	    helper_n = 10 - i;
	    for (; i >= 4; i--)
	      if (cfun->machine->need_to_save [i])
		{
		  helper_n = 0;
		  break;
		}
	    break;
	  }
    }
  */

  emit_insn (gen_epilogue_start_marker ());

  /* TODO: do we need something similar in tiny? like emit call to __exit in tinystdlib
   * which shut down the tiny with halt
   * this is to emit refsym which say to linker to add call to __exit at the end
   * of the main(), we don't use it in tiny for now */
#if 0
  if (cfun->decl && strcmp (IDENTIFIER_POINTER (DECL_NAME (cfun->decl)), "main") == 0)
    emit_insn (gen_tiny_refsym_need_exit ());
#endif

#if 0
    if (is_eh)
    {
    gcc_assert("epilogue exception handling" && 0);
      /* We need to add the right "SP" register save just after the
	 regular ones, so that when we pop it off we're in the EH
	 return frame, not this one.  This overwrites our own return
	 address, but we're not going to be returning anyway.  */
      rtx r12 = gen_rtx_REG (Pmode, 12);
      rtx (*addPmode)(rtx, rtx, rtx) = TARGET_LARGE ? gen_addpsi3 : gen_addhi3;

      /* R12 will hold the new SP.  */
      i = cfun->machine->framesize_regs;
      emit_move_insn (r12, stack_pointer_rtx);
      emit_insn (addPmode (r12, r12, EH_RETURN_STACKADJ_RTX));
      emit_insn (addPmode (r12, r12, GEN_INT (i)));
      emit_move_insn (gen_rtx_MEM (Pmode, plus_constant (Pmode, stack_pointer_rtx, i)), r12);
    }
#endif

  int offset = 8;
  for (i = 0; i < FIRST_PSEUDO_REGISTER; i++) {

    /* FP is handled in special way */
    if (i == HARD_FRAME_POINTER_REGNUM)
        continue;

    if (cfun->machine->need_to_save [i]) {

#if 0
	if (tinyx)
	  {
	    /* Note: With TARGET_LARGE we still use
	       POPM as POPX.A is two bytes bigger.  */
	    emit_insn (gen_popm (stack_pointer_rtx, GEN_INT (seq - 1),
				 GEN_INT (count)));
	    i += count - 1;
	  }
	else if (i == 11 - helper_n
		 && ! tiny_is_interrupt_func ()
		 && ! is_reentrant_func ()
		 && ! is_critical_func ()
		 && crtl->args.pretend_args_size == 0
		 /* Calling the helper takes as many bytes as the POP;RET sequence.  */
		 && helper_n > 1
		 && !is_eh)
	  {
	    emit_insn (gen_epilogue_helper (GEN_INT (helper_n)));
	    return;
	  }
	else
	  emit_insn (gen_pop (gen_rtx_REG (Pmode, i)));
#endif
        rtx addr = gen_rtx_PLUS (SImode, stack_pointer_rtx, GEN_INT (cfun->machine->framesize - offset));
        F (emit_insn (gen_rtx_SET (gen_rtx_REG (SImode, i),
                gen_rtx_MEM (SImode, addr))));
        offset += 4;
    }
  }

  fs = cfun->machine->framesize_locals + cfun->machine->framesize_outgoing
        + cfun->machine->framesize_regs;

  rtx sp = stack_pointer_rtx;
  if (frame_pointer_needed) {
    rtx addr = gen_rtx_PLUS (SImode, sp, GEN_INT (cfun->machine->framesize - 4));
    F (emit_insn (gen_rtx_SET (gen_rtx_REG (SImode, HARD_FRAME_POINTER_REGNUM),
        gen_rtx_MEM (SImode, addr))));
    DEBUG_GCC(fprintf (stderr, "FRAME: frame_pointer_needed\n"));
    //F (emit_move_insn (gen_rtx_REG (Pmode, HARD_FRAME_POINTER_REGNUM), sp));
  }

  increment_stack (fs);
#if 0
  if (is_eh)
    {
      /* Also pop SP, which puts us into the EH return frame.  Except
	 that you can't "pop" sp, you have to just load it off the
	 stack.  */
      emit_move_insn (stack_pointer_rtx, gen_rtx_MEM (Pmode, stack_pointer_rtx));
    }

  if (crtl->args.pretend_args_size)
    emit_insn (gen_swap_and_shrink ());

  if (is_critical_func ())
    emit_insn (gen_pop_intr_state ());
  else if (is_reentrant_func ())
    emit_insn (gen_enable_interrupts ());
#endif
  emit_jump_insn (gen_msp_return ());
}

#undef  TARGET_INIT_DWARF_REG_SIZES_EXTRA
#define TARGET_INIT_DWARF_REG_SIZES_EXTRA tiny_init_dwarf_reg_sizes_extra
void
tiny_init_dwarf_reg_sizes_extra (tree address)
{
  int i;
  rtx addr = expand_normal (address);
  rtx mem = gen_rtx_MEM (BLKmode, addr);

  if (!tinyx)
    return;

  for (i = 0; i < FIRST_PSEUDO_REGISTER; i++)
    {
      unsigned int dnum = DWARF_FRAME_REGNUM (i);
      unsigned int rnum = DWARF2_FRAME_REG_OUT (dnum, 1);

      if (rnum < DWARF_FRAME_REGISTERS)
	{
	  HOST_WIDE_INT offset = rnum * GET_MODE_SIZE (QImode);

	  emit_move_insn (adjust_address (mem, QImode, offset),
			  gen_int_mode (4, QImode));
	}
    }
}

/* Called by cbranch<mode>4 to coerce operands into usable forms.  */
void
tiny_fixup_compare_operands (machine_mode my_mode, rtx * operands)
{
  gcc_assert(0);

  /* constants we're looking for, not constants which are allowed.  */
  int const_op_idx = 1;

  if (tiny_reversible_cmp_operator (operands[0], VOIDmode))
    const_op_idx = 2;

  if (GET_CODE (operands[const_op_idx]) != REG
      && GET_CODE (operands[const_op_idx]) != MEM)
    operands[const_op_idx] = copy_to_mode_reg (my_mode, operands[const_op_idx]);
}

/* Simplify_gen_subreg() doesn't handle memory references the way we
   need it to below, so we use this function for when we must get a
   valid subreg in a "natural" state.  */
rtx
tiny_subreg (machine_mode mode, rtx r, machine_mode omode, int byte)
{
  /* FIXME: do we need this?
   */
  gcc_assert(0);

  rtx rv;

  if (GET_CODE (r) == SUBREG
      && SUBREG_BYTE (r) == 0)
    {
      rtx ireg = SUBREG_REG (r);
      machine_mode imode = GET_MODE (ireg);

      /* special case for (HI (SI (PSI ...), 0)) */
      if (imode == PSImode
	  && mode == HImode
	  && byte == 0)
	rv = gen_rtx_SUBREG (mode, ireg, byte);
      else
	rv = simplify_gen_subreg (mode, ireg, imode, byte);
    }
  else if (GET_CODE (r) == MEM)
    rv = adjust_address (r, mode, byte);
  else
    rv = simplify_gen_subreg (mode, r, omode, byte);

  if (!rv)
    gcc_unreachable ();

  return rv;
}

/* Called by movsi_x to generate the HImode operands.  */
void
tiny_split_movsi (rtx *operands)
{

  gcc_assert(0);

  rtx op00, op02, op10, op12;

  op00 = tiny_subreg (HImode, operands[0], SImode, 0);
  op02 = tiny_subreg (HImode, operands[0], SImode, 2);

  if (GET_CODE (operands[1]) == CONST
      || GET_CODE (operands[1]) == SYMBOL_REF)
    {
      op10 = gen_rtx_ZERO_EXTRACT (HImode, operands[1], GEN_INT (16), GEN_INT (0));
      op10 = gen_rtx_CONST (HImode, op10);
      op12 = gen_rtx_ZERO_EXTRACT (HImode, operands[1], GEN_INT (16), GEN_INT (16));
      op12 = gen_rtx_CONST (HImode, op12);
    }
  else
    {
      op10 = tiny_subreg (HImode, operands[1], SImode, 0);
      op12 = tiny_subreg (HImode, operands[1], SImode, 2);
    }

  if (rtx_equal_p (operands[0], operands[1]))
    {
      operands[2] = op02;
      operands[4] = op12;
      operands[3] = op00;
      operands[5] = op10;
    }
  else if (rtx_equal_p (op00, op12)
	   /* Catch the case where we are loading (rN, rN+1) from mem (rN).  */
	   || (REG_P (op00) && reg_mentioned_p (op00, op10))
	   /* Or storing (rN) into mem (rN).  */
	   || (REG_P (op10) && reg_mentioned_p (op10, op00))
	   )
    {
      operands[2] = op02;
      operands[4] = op12;
      operands[3] = op00;
      operands[5] = op10;
    }
  else
    {
      operands[2] = op00;
      operands[4] = op10;
      operands[3] = op02;
      operands[5] = op12;
    }
}


/* The MSPABI specifies the names of various helper functions, many of
   which are compatible with GCC's helpers.  This table maps the GCC
   name to the MSPABI name.  */
static const struct
{
  char const * const gcc_name;
  char const * const ti_name;
}
  helper_function_name_mappings [] =
{
  /* Floating point to/from integer conversions.  */
  { "__truncdfsf2", "__mspabi_cvtdf" },
  { "__extendsfdf2", "__mspabi_cvtfd" },
  { "__fixdfhi", "__mspabi_fixdi" },
  { "__fixdfsi", "__mspabi_fixdli" },
  { "__fixdfdi", "__mspabi_fixdlli" },
  { "__fixunsdfhi", "__mspabi_fixdu" },
  { "__fixunsdfsi", "__mspabi_fixdul" },
  { "__fixunsdfdi", "__mspabi_fixdull" },
  { "__fixsfhi", "__mspabi_fixfi" },
  { "__fixsfsi", "__mspabi_fixfli" },
  { "__fixsfdi", "__mspabi_fixflli" },
  { "__fixunsfhi", "__mspabi_fixfu" },
  { "__fixunsfsi", "__mspabi_fixful" },
  { "__fixunsfdi", "__mspabi_fixfull" },
  { "__floathisf", "__mspabi_fltif" },
  { "__floatsisf", "__mspabi_fltlif" },
  { "__floatdisf", "__mspabi_fltllif" },
  { "__floathidf", "__mspabi_fltid" },
  { "__floatsidf", "__mspabi_fltlid" },
  { "__floatdidf", "__mspabi_fltllid" },
  { "__floatunhisf", "__mspabi_fltuf" },
  { "__floatunsisf", "__mspabi_fltulf" },
  { "__floatundisf", "__mspabi_fltullf" },
  { "__floatunhidf", "__mspabi_fltud" },
  { "__floatunsidf", "__mspabi_fltuld" },
  { "__floatundidf", "__mspabi_fltulld" },

  /* Floating point comparisons.  */
  /* GCC uses individual functions for each comparison, TI uses one
     compare <=> function.  */

  /* Floating point arithmatic */
  { "__adddf3", "__mspabi_addd" },
  { "__addsf3", "__mspabi_addf" },
  { "__divdf3", "__mspabi_divd" },
  { "__divsf3", "__mspabi_divf" },
  { "__muldf3", "__mspabi_mpyd" },
  { "__mulsf3", "__mspabi_mpyf" },
  { "__subdf3", "__mspabi_subd" },
  { "__subsf3", "__mspabi_subf" },
  /* GCC does not use helper functions for negation */

  /* Integer multiply, divide, remainder.  */
  { "__mulhi3", "__mspabi_mpyi" },
  { "__mulsi3", "__mspabi_mpyl" },
  { "__muldi3", "__mspabi_mpyll" },
#if 0
  /* Clarify signed vs unsigned first.  */
  { "__mulhisi3", "__mspabi_mpysl" }, /* gcc doesn't use widening multiply (yet?) */
  { "__mulsidi3", "__mspabi_mpysll" }, /* gcc doesn't use widening multiply (yet?) */
#endif

  { "__divhi3", "__mspabi_divi" },
  { "__divsi3", "__mspabi_divli" },
  { "__divdi3", "__mspabi_divlli" },
  { "__udivhi3", "__mspabi_divu" },
  { "__udivsi3", "__mspabi_divlu" },
  { "__udivdi3", "__mspabi_divllu" },
  { "__modhi3", "__mspabi_remi" },
  { "__modsi3", "__mspabi_remli" },
  { "__moddi3", "__mspabi_remlli" },
  { "__umodhi3", "__mspabi_remu" },
  { "__umodsi3", "__mspabi_remul" },
  { "__umoddi3", "__mspabi_remull" },

  /* Bitwise operations.  */
  /* Rotation - no rotation support yet.  */
  /* Logical left shift - gcc already does these itself.  */
  /* Arithmetic left shift - gcc already does these itself.  */
  /* Arithmetic right shift - gcc already does these itself.  */

  { NULL, NULL }
};

/* Returns true if the current MCU supports an F5xxx series
   hardware multiper.  */

bool
tiny_use_f5_series_hwmult (void)
{
  static const char * cached_match = NULL;
  static bool         cached_result;

  if (tiny_hwmult_type == F5SERIES)
    return true;

  if (target_mcu == NULL || tiny_hwmult_type != AUTO)
    return false;

  if (target_mcu == cached_match)
    return cached_result;

  cached_match = target_mcu;

  if (strncasecmp (target_mcu, "tinyf5", 8) == 0)
    return cached_result = true;
  if (strncasecmp (target_mcu, "tinyfr5", 9) == 0)
    return cached_result = true;
  if (strncasecmp (target_mcu, "tinyf6", 8) == 0)
    return cached_result = true;

  static const char * known_f5_mult_mcus [] =
    {
      "cc430f5123",	"cc430f5125",	"cc430f5133",
      "cc430f5135",	"cc430f5137",	"cc430f5143",
      "cc430f5145",	"cc430f5147",	"cc430f6125",
      "cc430f6126",	"cc430f6127",	"cc430f6135",
      "cc430f6137",	"cc430f6143",	"cc430f6145",
      "cc430f6147",	"tinybt5190",	"tinysl5438a",
      "tinyxgeneric"
    };
  int i;

  for (i = ARRAY_SIZE (known_f5_mult_mcus); i--;)
    if (strcasecmp (target_mcu, known_f5_mult_mcus[i]) == 0)
      return cached_result = true;

  return cached_result = false;
}

/* Returns true if the current MCU has a second generation
   32-bit hardware multiplier.  */

static bool
use_32bit_hwmult (void)
{
  static const char * known_32bit_mult_mcus [] =
    {
      "tinyf4783",      "tinyf4793",      "tinyf4784",
      "tinyf4794",      "tinyf47126",     "tinyf47127",
      "tinyf47163",     "tinyf47173",     "tinyf47183",
      "tinyf47193",     "tinyf47166",     "tinyf47176",
      "tinyf47186",     "tinyf47196",     "tinyf47167",
      "tinyf47177",     "tinyf47187",     "tinyf47197"
    };
  static const char * cached_match = NULL;
  static bool         cached_result;
  int i;

  if (tiny_hwmult_type == LARGE)
    return true;

  if (target_mcu == NULL || tiny_hwmult_type != AUTO)
    return false;

  if (target_mcu == cached_match)
    return cached_result;

  cached_match = target_mcu;
  for (i = ARRAY_SIZE (known_32bit_mult_mcus); i--;)
    if (strcasecmp (target_mcu, known_32bit_mult_mcus[i]) == 0)
      return cached_result = true;

  return cached_result = false;
}

/* Returns true if the current MCU does not have a
   hardware multiplier of any kind.  */

static bool
tiny_no_hwmult (void)
{
  static const char * known_nomult_mcus [] =
    {
      "tinyc091",	"tinyc092",	"tinyc111",
      "tinyc1111", 	"tinyc112", 	"tinyc1121",
      "tinyc1331", 	"tinyc1351", 	"tinyc311s",
      "tinyc312", 	"tinyc313", 	"tinyc314",
      "tinyc315", 	"tinyc323", 	"tinyc325",
      "tinyc412", 	"tinyc413", 	"tinye112",
      "tinye313", 	"tinye315", 	"tinye325",
      "tinyf110", 	"tinyf1101", 	"tinyf1101a",
      "tinyf1111", 	"tinyf1111a",	"tinyf112",
      "tinyf1121", 	"tinyf1121a", "tinyf1122",
      "tinyf1132", 	"tinyf122", 	"tinyf1222",
      "tinyf123", 	"tinyf1232", 	"tinyf133",
      "tinyf135", 	"tinyf155", 	"tinyf156",
      "tinyf157", 	"tinyf2001", 	"tinyf2002",
      "tinyf2003", 	"tinyf2011", 	"tinyf2012",
      "tinyf2013", 	"tinyf2101", 	"tinyf2111",
      "tinyf2112", 	"tinyf2121", 	"tinyf2122",
      "tinyf2131", 	"tinyf2132", 	"tinyf2232",
      "tinyf2234", 	"tinyf2252", 	"tinyf2254",
      "tinyf2272", 	"tinyf2274", 	"tinyf412",
      "tinyf413", 	"tinyf4132", 	"tinyf415",
      "tinyf4152", 	"tinyf417", 	"tinyf4250",
      "tinyf4260", 	"tinyf4270", 	"tinyf435",
      "tinyf4351", 	"tinyf436", 	"tinyf4361",
      "tinyf437", 	"tinyf4371", 	"tinyf438",
      "tinyf439", 	"tinyf477", 	"tinyf478",
      "tinyf479", 	"tinyfe423", 	"tinyfe4232",
      "tinyfe423a",   "tinyfe4242",	"tinyfe425",
      "tinyfe4252",   "tinyfe425a", "tinyfe427",
      "tinyfe4272",   "tinyfe427a", "tinyfg4250",
      "tinyfg4260",   "tinyfg4270", "tinyfg437",
      "tinyfg438", 	"tinyfg439", 	"tinyfg477",
      "tinyfg478", 	"tinyfg479",  "tinyfr2032",
      "tinyfr2033",	"tinyfr4131",	"tinyfr4132",
      "tinyfr4133",	"tinyfw423",  "tinyfw425",
      "tinyfw427", 	"tinyfw428",  "tinyfw429",
      "tinyg2001", 	"tinyg2101",  "tinyg2102",
      "tinyg2111", 	"tinyg2112",  "tinyg2113",
      "tinyg2121", 	"tinyg2131",  "tinyg2132",
      "tinyg2152", 	"tinyg2153",  "tinyg2201",
      "tinyg2202", 	"tinyg2203",  "tinyg2210",
      "tinyg2211", 	"tinyg2212",  "tinyg2213",
      "tinyg2221", 	"tinyg2230",  "tinyg2231",
      "tinyg2232", 	"tinyg2233",  "tinyg2252",
      "tinyg2253", 	"tinyg2302",  "tinyg2303",
      "tinyg2312", 	"tinyg2313",  "tinyg2332",
      "tinyg2333", 	"tinyg2352",  "tinyg2353",
      "tinyg2402", 	"tinyg2403",  "tinyg2412",
      "tinyg2413", 	"tinyg2432",  "tinyg2433",
      "tinyg2444", 	"tinyg2452",  "tinyg2453",
      "tinyg2513", 	"tinyg2533",  "tinyg2544",
      "tinyg2553", 	"tinyg2744",  "tinyg2755",
      "tinyg2855", 	"tinyg2955",  "tinyl092",
      "tinyp112", 	"tinyp313",   "tinyp315",
      "tinyp315s", 	"tinyp325",   "tinytch5e"
    };
  static const char * cached_match = NULL;
  static bool         cached_result;
  int i;

  if (tiny_hwmult_type == NONE)
    return true;

  if (target_mcu == NULL || tiny_hwmult_type != AUTO)
    return false;

  if (target_mcu == cached_match)
    return cached_result;

  cached_match = target_mcu;
  for (i = ARRAY_SIZE (known_nomult_mcus); i--;)
    if (strcasecmp (target_mcu, known_nomult_mcus[i]) == 0)
      return cached_result = true;

  return cached_result = false;
}

/* This function does the same as the default, but it will replace GCC
   function names with the MSPABI-specified ones.  */

void
tiny_output_labelref (FILE *file, const char *name)
{
  int i;

  for (i = 0; helper_function_name_mappings [i].gcc_name; i++)
    if (strcmp (helper_function_name_mappings [i].gcc_name, name) == 0)
      {
	name = helper_function_name_mappings [i].ti_name;
	break;
      }

  /* If we have been given a specific MCU name then we may be
     able to make use of its hardware multiply capabilities.  */
  if (tiny_hwmult_type != NONE)
    {
      if (strcmp ("__mspabi_mpyi", name) == 0)
	{
	  if (tiny_use_f5_series_hwmult ())
	    name = "__mulhi2_f5";
	  else if (! tiny_no_hwmult ())
	    name = "__mulhi2";
	}
      else if (strcmp ("__mspabi_mpyl", name) == 0)
	{
	  if (tiny_use_f5_series_hwmult ())
	    name = "__mulsi2_f5";
	  else if (use_32bit_hwmult ())
	    name = "__mulsi2_hw32";
	  else if (! tiny_no_hwmult ())
	    name = "__mulsi2";
	}
    }

  fputs (name, file);
}

/* Common code for tiny_print_operand...  */

static void
tiny_print_operand_raw (FILE * file, rtx op)
{
  HOST_WIDE_INT i;

  DEBUG_GCC(
    fprintf(stderr, "tiny_print_operand_raw: ");
    debug_rtx(op)
  );

  switch (GET_CODE (op))
    {
    case REG:
      DEBUG_GCC(fprintf(stderr, "REG"));
      fprintf (file, "%s", reg_names [REGNO (op)]);
      break;

    case CONST_INT:
      DEBUG_GCC(fprintf(stderr, "CONST_INT"));
      i = INTVAL (op);
      if (TARGET_ASM_HEX)
	fprintf (file, "%#" HOST_WIDE_INT_PRINT "x", i);
      else
	fprintf (file, "%" HOST_WIDE_INT_PRINT "d", i);
      break;

    case CONST:
      DEBUG_GCC(fprintf(stderr, "CONST"));
    case PLUS:
    case MINUS:
    case SYMBOL_REF:
    case LABEL_REF:
      output_addr_const (file, op);
      break;

    default:
      print_rtl (file, op);
      break;
    }
}

#undef  TARGET_PRINT_OPERAND_ADDRESS
#define TARGET_PRINT_OPERAND_ADDRESS	tiny_print_operand_addr

/* Output to stdio stream FILE the assembler syntax for an
   instruction operand that is a memory reference whose address
   is ADDR.  */

static void
tiny_print_operand_addr (FILE * file, machine_mode mode, rtx addr)
{
  switch (GET_CODE (addr))
    {
    case PLUS:
      tiny_print_operand_raw (file, XEXP (addr, 1));
      gcc_assert (REG_P (XEXP (addr, 0)));
      fprintf (file, "(%s)", reg_names [REGNO (XEXP (addr, 0))]);
      return;

    case REG:
      fprintf (file, "0(");
      tiny_print_operand_raw (file, addr);
      fprintf (file, ")");
      return;

    case CONST:
    case CONST_INT:
    case SYMBOL_REF:
    case LABEL_REF:
      debug_rtx(addr);
      exit(0);
      fprintf (file, "&");
      break;

    default:
      break;
    }

  tiny_print_operand_raw (file, addr);
}

#undef  TARGET_PRINT_OPERAND
#define TARGET_PRINT_OPERAND		tiny_print_operand

/* A   low 16-bits of int/lower of register pair
   B   high 16-bits of int/higher of register pair
   C   bits 32-47 of a 64-bit value/reg 3 of a DImode value
   D   bits 48-63 of a 64-bit value/reg 4 of a DImode value
   H   like %B (for backwards compatibility)
   I   inverse of value
   J   an integer without a # prefix
   L   like %A (for backwards compatibility)
   O   offset of the top of the stack
   Q   like X but generates an A postfix
   R   inverse of condition code, unsigned.
   X   X instruction postfix in large mode
   Y   value - 4
   Z   value - 1
   b   .B or .W or .A, depending upon the mode
   p   bit position
   r   inverse of condition code
   x   like X but only for pointers.  */

static void
tiny_print_operand (FILE * file, rtx op, int letter)
{
  rtx addr;

  DEBUG_GCC(
    fprintf(stderr, "tiny_print_operand");
    debug_rtx(op)
  );

  /* We can't use c, n, a, or l.  */
  switch (letter)
    {
    case 'Z':
      gcc_assert (CONST_INT_P (op));
      /* Print the constant value, less one.  */
      fprintf (file, "#%ld", INTVAL (op) - 1);
      return;
    case 'Y':
      gcc_assert (CONST_INT_P (op));
      /* Print the constant value, less four.  */
      fprintf (file, "#%ld", INTVAL (op) - 4);
      return;
    case 'I':
      if (GET_CODE (op) == CONST_INT)
	{
	  /* Inverse of constants */
	  int i = INTVAL (op);
	  fprintf (file, "%d", ~i);
	  return;
	}
      op = XEXP (op, 0);
      break;
    case 'r': /* Conditional jump where the condition is reversed.  */
      switch (GET_CODE (op))
	{
	case EQ: fprintf (file, "NE"); break;
	case NE: fprintf (file, "EQ"); break;
	case GEU: fprintf (file, "LO"); break;
	case LTU: fprintf (file, "HS"); break;
	case GE: fprintf (file, "L"); break;
	case LT: fprintf (file, "GE"); break;
	  /* Assume these have reversed operands.  */
	case GTU: fprintf (file, "HS"); break;
	case LEU: fprintf (file, "LO"); break;
	case GT: fprintf (file, "GE"); break;
	case LE: fprintf (file, "L"); break;
	default:
	  tiny_print_operand_raw (file, op);
	  break;
	}
      return;
    case 'R': /* Conditional jump where the operands are reversed.  */
      switch (GET_CODE (op))
	{
    gcc_assert(0);
	case GTU: fprintf (file, "LO"); break;
	case LEU: fprintf (file, "HS"); break;
	case GT: fprintf (file, "L"); break;
	case LE: fprintf (file, "GE"); break;
	default:
	  tiny_print_operand_raw (file, op);
	  break;
	}
      return;
    case 'p': /* Bit position. 0 == 0x01, 3 = 0x08 etc.  */
      gcc_assert (CONST_INT_P (op));
      fprintf (file, "#%d", 1 << INTVAL (op));
      return;
    case 'b':
      switch (GET_MODE (op))
	{
	case QImode: fprintf (file, ".B"); return;
	case HImode: fprintf (file, ".W"); return;
	case PSImode: fprintf (file, ".A"); return;
	case SImode: fprintf (file, ".A"); return;
	default:
	  return;
	}
    case 'A':
    case 'L': /* Low half.  */
      switch (GET_CODE (op))
	{
	case MEM:
	  op = adjust_address (op, Pmode, 0);
	  break;
	case REG:
	  break;
	case CONST_INT:
	  op = GEN_INT (INTVAL (op) & 0xffff);
	  letter = 0;
	  break;
	default:
	  /* If you get here, figure out a test case :-) */
	  gcc_unreachable ();
	}
      break;
    case 'B':
    case 'H': /* high half */
      switch (GET_CODE (op))
	{
	case MEM:
	  op = adjust_address (op, Pmode, 2);
	  break;
	case REG:
	  op = gen_rtx_REG (Pmode, REGNO (op) + 1);
	  break;
	case CONST_INT:
	  op = GEN_INT (INTVAL (op) >> 16);
	  letter = 0;
	  break;
	default:
	  /* If you get here, figure out a test case :-) */
	  gcc_unreachable ();
	}
      break;
    case 'C':
      switch (GET_CODE (op))
	{
	case MEM:
	  op = adjust_address (op, Pmode, 3);
	  break;
	case REG:
	  op = gen_rtx_REG (Pmode, REGNO (op) + 2);
	  break;
	case CONST_INT:
	  op = GEN_INT ((long long) INTVAL (op) >> 32);
	  letter = 0;
	  break;
	default:
	  /* If you get here, figure out a test case :-) */
	  gcc_unreachable ();
	}
      break;
    case 'D':
      switch (GET_CODE (op))
	{
	case MEM:
	  op = adjust_address (op, Pmode, 4);
	  break;
	case REG:
	  op = gen_rtx_REG (Pmode, REGNO (op) + 3);
	  break;
	case CONST_INT:
	  op = GEN_INT ((long long) INTVAL (op) >> 48);
	  letter = 0;
	  break;
	default:
	  /* If you get here, figure out a test case :-) */
	  gcc_unreachable ();
	}
      break;

    case 'X':
      /* This is used to turn, for example, an ADD opcode into an ADDX
	 opcode when we're using 20-bit addresses.  */
      if (TARGET_LARGE || GET_MODE (op) == PSImode)
	fprintf (file, "X");
      /* We don't care which operand we use, but we want 'X' in the MD
	 file, so we do it this way.  */
      return;

    case 'x':
      /* Similarly, but only for PSImodes.  BIC, for example, needs this.  */
      if (GET_MODE (op) == PSImode)
	fprintf (file, "X");
      return;

    case 'Q':
      /* Likewise, for BR -> BRA.  */
      if (TARGET_LARGE)
	fprintf (file, "A");
      return;

    case 'O':
      /* Computes the offset to the top of the stack for the current frame.
	 This has to be done here rather than in, say, tiny_expand_builtin()
	 because builtins are expanded before the frame layout is determined.  */
      gcc_assert(0);
      fprintf (file, "%d",
	       tiny_initial_elimination_offset (HARD_FRAME_POINTER_REGNUM, STACK_POINTER_REGNUM) - 4);
      return;

    case 'J':
      gcc_assert (GET_CODE (op) == CONST_INT);
    /* TODO: this shoudl be cleared, this is for line 2456 */
    case 'o':
    case 0:
      break;
    default:
      output_operand_lossage ("invalid operand prefix");
      return;
    }

  switch (GET_CODE (op))
    {
    case REG:
      tiny_print_operand_raw (file, op);
      break;

    case MEM:
      addr = XEXP (op, 0);
      tiny_print_operand_addr (file, GET_MODE(op), addr);
      break;

    case CONST:
      if (GET_CODE (XEXP (op, 0)) == ZERO_EXTRACT)
	{
	  op = XEXP (op, 0);
	  switch (INTVAL (XEXP (op, 2)))
	    {
	    case 0:
	      fprintf (file, "#lo (");
	      tiny_print_operand_raw (file, XEXP (op, 0));
	      fprintf (file, ")");
	      break;
	  
	    case 16:
	      fprintf (file, "#hi (");
	      tiny_print_operand_raw (file, XEXP (op, 0));
	      fprintf (file, ")");
	      break;

	    default:
	      output_operand_lossage ("invalid zero extract");
	      break;
	    }
	  break;
	}
      /* Fall through.  */
    case CONST_INT:
        /* XXX ldc if (letter == 0)
	        fprintf (file, "#");*/
        tiny_print_operand_raw (file, op);
	    break;
    case SYMBOL_REF:
    case LABEL_REF:
/*
      if (letter == 0)
	    //fprintf (file, "#");
      else
	    fprintf (file, "_%c", letter);
*/
      tiny_print_operand_raw (file, op);
      break;

    case EQ: fprintf (file, "EQ"); break;
    case NE: fprintf (file, "NE"); break;
    case LE: fprintf (file, "LE"); break;
    case GEU: fprintf (file, "GEU"); break;
    case GTU: fprintf (file, "GU"); break;
    case LTU: fprintf (file, "LO"); break;
    case GE: fprintf (file, "GE"); break;
    case GT: fprintf (file, "GT"); break;
    case LT: fprintf (file, "LT"); break;

    default:
      print_rtl (file, op);
      break;
    }
}

/* Frame stuff.  */

rtx
tiny_return_addr_rtx (int count)
{
  int ra_size;
  if (count)
    return NULL_RTX;

  ra_size = TARGET_LARGE ? 4 : 2;
  if (crtl->args.pretend_args_size)
    ra_size += 2;

  return gen_rtx_MEM (Pmode, gen_rtx_PLUS (Pmode, arg_pointer_rtx, GEN_INT (- ra_size)));
}

rtx
tiny_incoming_return_addr_rtx (void)
{
  return gen_rtx_MEM (Pmode, stack_pointer_rtx);
}

/* Instruction generation stuff.  */

/* Generate a sequence of instructions to sign-extend an HI
   value into an SI value.  Handles the tricky case where
   we are overwriting the destination.  */

const char *
tinyx_extendhisi (rtx * operands)
{
  if (REGNO (operands[0]) == REGNO (operands[1]))
    /* Low word of dest == source word.  */
    return "BIT.W\t#0x8000, %L0 { SUBC.W\t%H0, %H0 { INV.W\t%H0, %H0"; /* 8-bytes.  */

  if (! tinyx)
    /* Note: This sequence is approximately the same length as invoking a helper
       function to perform the sign-extension, as in:

         MOV.W  %1, %L0
	 MOV.W  %1, r12
	 CALL   __mspabi_srai_15
	 MOV.W  r12, %H0

       but this version does not involve any function calls or using argument
       registers, so it reduces register pressure.  */
    return "MOV.W\t%1, %L0 { BIT.W\t#0x8000, %L0 { SUBC.W\t%H0, %H0 { INV.W\t%H0, %H0"; /* 10-bytes.  */

  if (REGNO (operands[0]) + 1 == REGNO (operands[1]))
    /* High word of dest == source word.  */
    return "MOV.W\t%1, %L0 { RPT\t#15 { RRAX.W\t%H0"; /* 6-bytes.  */

  /* No overlap between dest and source.  */
  return "MOV.W\t%1, %L0 { MOV.W\t%1, %H0 { RPT\t#15 { RRAX.W\t%H0"; /* 8-bytes.  */
}

/* Likewise for logical right shifts.  */
const char *
tinyx_logical_shift_right (rtx amount)
{
  gcc_assert(0);
  /* The TINYX's logical right shift instruction - RRUM - does
     not use an extension word, so we cannot encode a repeat count.
     Try various alternatives to work around this.  If the count
     is in a register we are stuck, hence the assert.  */
  gcc_assert (CONST_INT_P (amount));

  if (INTVAL (amount) <= 0
      || INTVAL (amount) >= 16)
    return "# nop logical shift.";

  if (INTVAL (amount) > 0
      && INTVAL (amount) < 5)
    return "rrum.w\t%2, %0"; /* Two bytes.  */

  if (INTVAL (amount) > 4
      && INTVAL (amount) < 9)
    return "rrum.w\t#4, %0 { rrum.w\t%Y2, %0 "; /* Four bytes.  */

  /* First we logically shift right by one.  Now we know
     that the top bit is zero and we can use the arithmetic
     right shift instruction to perform the rest of the shift.  */
  return "rrum.w\t#1, %0 { rpt\t%Z2 { rrax.w\t%0"; /* Six bytes.  */
}

struct gcc_target targetm = TARGET_INITIALIZER;

#include "gt-tiny.h"
